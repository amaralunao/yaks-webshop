from decimal import Decimal

from django.forms.models import model_to_dict
from django.test import TestCase

from ..models import THREE_PLACES, TWO_PLACES, Stock
from ..serializers import (LabYakDetailsOutputSerializer, OrderInputSerializer,
                           OrderOutputSerializer)
from .factories import LabYakFactory, OrderFactory


class TestLabYakDetailsOutputSerializer(TestCase):

    def setUp(self):
        self.yak = LabYakFactory.build(name="Betty-1",
                                       age=Decimal(4.0),
                                       sex="f")
        self.user_data = model_to_dict(self.yak)

    def test_serializer_correct_data(self):
        serializer = LabYakDetailsOutputSerializer(self.yak,
                                                   context=dict(time_elapsed_in_days=13))
        self.assertEqual(serializer.data.get('age'), Decimal('4.13').quantize(TWO_PLACES))
        self.assertEqual(serializer.data.get('age_last_shaved'), Decimal('4.00').quantize(TWO_PLACES))
        self.assertEqual(serializer.data.get('name'), 'Betty-1')


class TestOrderInputSerializer(TestCase):

    def setUp(self):
        self.stock = Stock(milk=Decimal(1188.81).quantize(THREE_PLACES),
                           skins=3)
        self.data = {}
        self.data['customer'] = 'YakAfficionado'

    def test_save_order_quanitities_in_stock(self):
        self.data['order'] = {'milk': 1100, 'skins': 3}
        context = dict(stock=self.stock, time_elapsed_in_days=14)
        serializer = OrderInputSerializer(data=self.data, context=context)
        self.assertTrue(serializer.is_valid())
        order = serializer.save()
        self.assertEqual(order.milk, Decimal(self.data['order']['milk']).quantize(THREE_PLACES))
        self.assertEqual(order.skins, self.data['order']['skins'])

    def test_save_order_partial_in_stock(self):
        self.data['order'] = {'milk': 1200, 'skins': 3}
        context = dict(stock=self.stock, time_elapsed_in_days=14)
        serializer = OrderInputSerializer(data=self.data, context=context)
        self.assertTrue(serializer.is_valid())
        order = serializer.save()
        self.assertIsNone(order.milk)
        self.assertEqual(order.skins, self.data['order']['skins'])

    def test_save_order_not_in_stock(self):
        self.data['order'] = {'milk': 1200, 'skins': 5}
        context = dict(stock=self.stock, time_elapsed_in_days=14)
        serializer = OrderInputSerializer(data=self.data, context=context)
        self.assertTrue(serializer.is_valid())
        order = serializer.save()
        self.assertIsNone(order)


class TestOrderOutputSerializer(TestCase):

    def setUp(self):
        self.order = OrderFactory.build(milk=None, skins=3)

    def test_serializer_does_not_display_empty_fields(self):
        serializer = OrderOutputSerializer(self.order)
        self.assertEqual(serializer.data, dict(skins=3))
