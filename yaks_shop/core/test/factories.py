from decimal import Decimal

import factory

from ..models import THREE_PLACES, TWO_PLACES


class LabYakFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'core.LabYak'
        django_get_or_create = ('name',)

    id = factory.Sequence(lambda n: n)
    name = factory.Sequence(lambda n: f'Betty-{n}')
    age = Decimal(5).quantize(TWO_PLACES)
    sex = 'f'


class OrderFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'core.Order'
        django_get_or_create = ('customer_name',)

    id = factory.Sequence(lambda n: n)
    customer_name = factory.Sequence(lambda n: f'Afficionado-{n}')
    milk = Decimal(5).quantize(THREE_PLACES)
    skins = factory.Sequence(lambda n: n)
    time_elapsed_in_days = factory.Sequence(lambda n: n)
