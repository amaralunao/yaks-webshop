import os
from decimal import Decimal
from unittest.mock import MagicMock

import untangle
from django.test import TestCase
from django.urls import reverse
from nose.tools import eq_
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import LabYak
from ..views import get_or_create_herd_details


class TestFileUploadEndpoint(APITestCase):
    """
    Tests /yak-shop/upload-file/
    """
    def setUp(self):
        self.dir_absolut_path = os.path.dirname(__file__)
        self.xml_file = os.path.join(self.dir_absolut_path, 'herd.xml')
        self.url = reverse('upload-file')

    def test_file_upload_successful(self):
        with open(self.xml_file) as xml_file:
            self.post_data = {"file": xml_file, "time_elapsed_in_days": 13}
            response = self.client.post(self.url, self.post_data)
            eq_(response.status_code, status.HTTP_200_OK)
            self.assertIn('Betty-1', response.content.decode())
            self.assertIn('3 skins of wool', response.content.decode())

    def test_file_upload_invalid_file_extension(self):
        other_file = os.path.join(self.dir_absolut_path, 'factories.py')
        with open(other_file) as of:
            self.post_data = {"file": of, "time_elapsed_in_days": 13}
            response = self.client.post(self.url, self.post_data)
            eq_(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn('Input Error', response.content.decode())

    def test_file_upload_empty_data(self):
        self.post_data = {}
        response = self.client.post(self.url, self.post_data)
        eq_(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Input Error', response.content.decode())


class TestGetOrCreateHerdDetails(TestCase):

    def setUp(self):
        self.dir_absolut_path = os.path.dirname(__file__)
        self.xml_file = os.path.join(self.dir_absolut_path, 'herd.xml')
        self.yak_details = untangle.parse(self.xml_file)

    def test_get_or_create_herd_details_with_save_to_database_and_delete_existing(self):
        LabYak.objects.create(name="Betty-1",
                              age=Decimal(4.0),
                              sex="f")
        queryset_before = LabYak.objects.all()
        eq_(queryset_before.count(), 1)
        _, yak_serializer_errors = get_or_create_herd_details(self.yak_details)
        eq_(yak_serializer_errors, [])
        queryset = LabYak.objects.all()
        eq_(queryset.count(), 3)

    def test_get_or_create_herd_details_without_save_to_database_and_no_delete(self):
        LabYak.objects.create(name="Betty-1",
                              age=Decimal(4.0),
                              sex="f")
        queryset_before = LabYak.objects.all()
        eq_(queryset_before.count(), 1)
        _, yak_serializer_errors = get_or_create_herd_details(self.yak_details,
                                                              delete_existing=False,
                                                              save_to_database=False)
        eq_(yak_serializer_errors, [])
        queryset = LabYak.objects.all()
        eq_(queryset.count(), 1)

    def test_get_or_create_herd_details_serializer_errors(self):
        queryset_before = LabYak.objects.all()
        eq_(queryset_before.count(), 0)
        yak = MagicMock(_attributes={'age': -1, 'sex': 's'})
        herd = MagicMock(labyak=[yak])
        yak_details = MagicMock(herd=herd)
        yaks, yak_serializer_errors = get_or_create_herd_details(yak_details)
        eq_(len(yaks), 0)
        self.assertEqual(len(yak_serializer_errors), 1)
        queryset = LabYak.objects.all()
        eq_(queryset.count(), 0)


class TestLadEndpoint(APITestCase):
    """
    Tests /yak-shop/load/
    """

    def setUp(self):
        self.url = reverse('load')

    def test_post_yak_payload_success(self):
        queryset_count_before = LabYak.objects.all().count()
        eq_(queryset_count_before, 0)
        data = """<herd><labyak name="Betty-1" age="4" sex="f"/><labyak name="Betty-2" age="8" sex="f"/><labyak name="Betty-3" age="9.5" sex="f"/></herd>"""
        response = self.client.post(self.url,
                                    data=data,
                                    content_type='application/xml')
        eq_(response.status_code, status.HTTP_205_RESET_CONTENT)
        queryset_count_after = LabYak.objects.all().count()
        eq_(queryset_count_after, 3)

    def test_post_yak_payload_success_delete_existing(self):
        LabYak.objects.create(name="Boo", age=Decimal(4.0), sex='m')
        queryset_count_before = LabYak.objects.all().count()
        eq_(queryset_count_before, 1)
        data = """<herd><labyak name="Betty-1" age="4" sex="f"/><labyak name="Betty-2" age="8" sex="f"/><labyak name="Betty-3" age="9.5" sex="f"/></herd>"""
        response = self.client.post(self.url,
                                    data=data,
                                    content_type='application/xml')
        eq_(response.status_code, status.HTTP_205_RESET_CONTENT)
        queryset_count_after = LabYak.objects.all().count()
        eq_(queryset_count_after, 3)
        eq_(LabYak.objects.filter(name="Boo").count(), 0)

    def test_post_yak_payload_serializer_errors(self):
        LabYak.objects.create(name="Boo", age=Decimal(4.0), sex='m')
        queryset_count_before = LabYak.objects.all().count()
        eq_(queryset_count_before, 1)
        data = """<herd><labyak name="Betty-1" age="notage" sex="f"/><labyak name="Betty-2" age="8" sex="f"/><labyak name="Betty-3" age="9.5" sex="f"/></herd>"""
        response = self.client.post(self.url,
                                    data=data,
                                    content_type='application/xml')
        eq_(response.status_code, status.HTTP_400_BAD_REQUEST)
        queryset_count_after = LabYak.objects.all().count()
        eq_(queryset_count_after, 1)
        eq_(LabYak.objects.filter(name="Boo").count(), 1)


class TestOrderView(APITestCase):

    def setUp(self):
        LabYak.objects.create(name="Betty-1",
                              age=Decimal(4.0),
                              sex="f")
        LabYak.objects.create(name="Betty-2",
                              age=Decimal(8.0),
                              sex="f")
        LabYak.objects.create(name="Betty-3",
                              age=Decimal(9.5),
                              sex="f")

        self.url = reverse('order', kwargs={'time_elapsed_in_days': 13})

    def test_order_post_success_created(self):
        data = {"customer": "Medvedev", "order": {"milk": 1100, "skins": 3}}
        response = self.client.post(self.url,
                                    data,
                                    format='json')
        eq_(response.status_code, status.HTTP_201_CREATED)
        eq_(response.data['order'].customer_name, 'Medvedev')
        eq_(response.data['order'].skins, 3)
        eq_(response.data['order'].milk, Decimal('1100.000'))

    def test_order_post_success_partial(self):
        data = {"customer": "Medvedev", "order": {"milk": 1100, "skins": 5}}
        response = self.client.post(self.url,
                                    data,
                                    format='json')
        eq_(response.status_code, status.HTTP_206_PARTIAL_CONTENT)

        eq_(response.data['order'].customer_name, 'Medvedev')
        self.assertIsNone(response.data['order'].skins)
        eq_(response.data['order'].milk, Decimal('1100.000'))
        self.assertIn('Partial', response.content.decode())

    def test_order_post_not_found(self):
        data = {"customer": "Medvedev", "order": {"milk": 1400, "skins": 5}}
        response = self.client.post(self.url,
                                    data,
                                    format='json')
        eq_(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIn('Order failed', response.content.decode())


class TestHerdView(APITestCase):

    def setUp(self):
        LabYak.objects.create(name="Betty-1",
                              age=Decimal(4.0),
                              sex="f")
        LabYak.objects.create(name="Betty-2",
                              age=Decimal(8.0),
                              sex="f")
        LabYak.objects.create(name="Betty-3",
                              age=Decimal(9.5),
                              sex="f")

        self.url = reverse('herd', kwargs={'time_elapsed_in_days': 13})

    def test_get_herd_success(self):
        response = self.client.get(self.url)
        eq_(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data['herd'])
        eq_(len(response.data['herd']), 3)


class TestStockView(APITestCase):

    def setUp(self):
        LabYak.objects.create(name="Betty-1",
                              age=Decimal(4.0),
                              sex="f")
        LabYak.objects.create(name="Betty-2",
                              age=Decimal(8.0),
                              sex="f")
        LabYak.objects.create(name="Betty-3",
                              age=Decimal(9.5),
                              sex="f")

        self.url = reverse('stock', kwargs={'time_elapsed_in_days': 13})

    def test_get_stock_success(self):
        response = self.client.get(self.url)
        eq_(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data['milk'])
        self.assertIsNotNone(response.data['skins'])
