from decimal import Decimal
from unittest.mock import MagicMock

from django.test import TestCase

from yaks_shop.core import models

from .factories import LabYakFactory


class TestLabYakModel(TestCase):

    def setUp(self):
        self.labyak = LabYakFactory.build(name='boo')

    def test_get_age_in_days_alive(self):
        self.assertEqual(self.labyak.age, Decimal(5.0))
        age_in_days = self.labyak.get_age_in_days(13)
        self.assertEqual(age_in_days, 513)

    def test_get_age_in_days_dead(self):
        labyak = LabYakFactory.build(age=Decimal(9.99))
        age_in_days = labyak.get_age_in_days(13)
        self.assertEqual(age_in_days, 1000)

    def test_get_age_in_yak_years(self):
        current_age_in_days = 456
        age_in_yak_years = self.labyak.get_age_in_yak_years(current_age_in_days)
        self.assertEqual(age_in_yak_years, Decimal(4.56).quantize(models.TWO_PLACES))

    def test_is_old_enough_to_shave_on_day_false(self):
        """
        This is testing a static method so yak's actual age
        is not relevant
        """
        is_old_enough = self.labyak.is_old_enough_to_shave_on_day(99)
        self.assertFalse(is_old_enough)

    def test_is_old_enough_to_shave_on_day_true(self):
        """
        This is testing a static method so yak's actual age
        is not relevant
        """
        is_old_enough = self.labyak.is_old_enough_to_shave_on_day(101)
        self.assertTrue(is_old_enough)

    def test_is_alive_on_day_false(self):
        """
        This is testing a static method so yak's actual age
        is not relevant
        """
        is_alive_on_day = self.labyak.is_alive_on_day(1001)
        self.assertFalse(is_alive_on_day)

    def test_is_alive_on_day_true(self):
        """
        This is testing a static method so yak's actual age
        is not relevant
        """
        is_alive_on_day = self.labyak.is_alive_on_day(987)
        self.assertTrue(is_alive_on_day)

    def test_is_eligible_for_shave_false_dead(self):
        labyak = self.labyak
        labyak.is_alive_on_day = MagicMock(return_value=False)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        self.assertFalse(labyak.is_eligible_for_shave(1234, 1233))

    def test_is_eligible_for_shave_false_days_between_shave(self):
        labyak = self.labyak
        labyak.is_alive_on_day = MagicMock(return_value=True)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        self.assertFalse(labyak.is_eligible_for_shave(1234, 1233))

    def test_is_eligible_for_shave_true(self):
        labyak = self.labyak
        labyak.is_alive_on_day = MagicMock(return_value=True)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        self.assertFalse(labyak.is_eligible_for_shave(513, 500))

    def test_get_wool_skins_and_age_last_shaved(self):
        labyak = self.labyak
        labyak.get_age_in_days = MagicMock(return_value=400)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        skins, age_last_shaved = self.labyak.get_wool_skins_and_age_last_shaved_in_days(2)
        self.assertEqual(skins, 1)
        self.assertEqual(age_last_shaved, 400)

    def test_get_wool_skins_and_age_last_shaved_more_thank_one_shave(self):
        labyak = self.labyak
        labyak.get_age_in_days = MagicMock(return_value=400)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        skins, age_last_shaved = self.labyak.get_wool_skins_and_age_last_shaved_in_days(14)
        self.assertEqual(skins, 2)
        self.assertEqual(age_last_shaved, 413)

    def test_get_wool_skins_and_age_last_shaved_not_shaving_before_day_is_over(self):
        labyak = self.labyak
        labyak.get_age_in_days = MagicMock(return_value=400)
        labyak.is_old_enough_to_shave_on_day = MagicMock(return_value=True)
        skins, age_last_shaved = self.labyak.get_wool_skins_and_age_last_shaved_in_days(13)
        self.assertEqual(skins, 1)
        self.assertEqual(age_last_shaved, 400)

    def test_get_milk_per_day_eligible(self):
        labyak = self.labyak
        labyak.is_eligible_for_milking = MagicMock(return_value=True)
        milk_per_day = labyak.get_milk_per_day(50)
        self.assertEqual(milk_per_day, Decimal(48.5).quantize(models.THREE_PLACES))

    def test_get_milk_per_day_not_eligible(self):
        labyak = self.labyak
        labyak.is_eligible_for_milking = MagicMock(return_value=False)
        milk_per_day = labyak.get_milk_per_day(50)
        self.assertEqual(milk_per_day, Decimal(0).quantize(models.THREE_PLACES))

    def test_get_total_milk_quantity(self):
        labyak = self.labyak
        labyak.is_eligible_for_milking = MagicMock(return_value=True)
        labyak.get_age_in_days = MagicMock(return_value=413)
        total_milk = labyak.get_total_milk_quantity(13)
        self.assertEqual(total_milk, Decimal(488.93).quantize(models.THREE_PLACES))


class TestStockModel(TestCase):

    def setUp(self):
        yak1 = LabYakFactory.build(name="Betty-1",
                                   age=Decimal(4.0),
                                   sex="f")
        yak2 = LabYakFactory.build(name="Betty-2",
                                   age=Decimal(8.0),
                                   sex="f")
        yak3 = LabYakFactory.build(name="Betty-3",
                                   age=Decimal(9.5),
                                   sex="f")

        self.labyaks = [yak1, yak2, yak3]

    def test_get_stock(self):
        stock = models.Stock(time_elapsed_in_days=13, yaks=self.labyaks)
        self.assertEqual(stock.milk, Decimal(1104.48).quantize(models.THREE_PLACES))
        self.assertEqual(stock.skins, 3)

    def test_get_stock_following_day(self):
        stock = models.Stock(time_elapsed_in_days=14, yaks=self.labyaks)
        self.assertEqual(stock.milk, Decimal(1188.81).quantize(models.THREE_PLACES))
        self.assertEqual(stock.skins, 4)
