from django.contrib import admin

# Register your models here.from django.contrib import admin
from .models import LabYak, Order


admin.site.register(LabYak)
admin.site.register(Order)
