# Create your views here.
from rest_framework import status, views
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer


from .models import Stock, LabYak
from .parsers import CustomXMLParser
from .serializers import (FileUploadSerializer,
                          LabYakSerializer, OrderInputSerializer,
                          OrderOutputSerializer, StatusOutputSerializer,
                          StockSerializer, HerdSerializer)


def get_or_create_herd_details(yak_details,
                               delete_existing=True,
                               save_to_database=True):
    yaks = list()
    yak_serializer_errors = list()
    valid_yaks = list()

    for yak in yak_details.herd.labyak:
        yak_serializer = LabYakSerializer(data=yak._attributes)
        if yak_serializer.is_valid():
            valid_yaks.append(LabYak(**yak_serializer.validated_data))
        else:
            yak_serializer_errors.append(yak_serializer.errors)

    if len(yak_serializer_errors) == 0:
        if delete_existing:
            LabYak.objects.all().delete()
        if save_to_database:
            yaks = LabYak.objects.bulk_create(valid_yaks)
        else:
            yaks = valid_yaks

    return yaks, yak_serializer_errors


class FileUploadView(views.APIView):
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (TemplateHTMLRenderer, )
    permission_classes = (AllowAny,)
    input_serializer = FileUploadSerializer
    output_serializer = StatusOutputSerializer
    template_name = 'status.html'

    def post(self, request, *args, **kwargs):
        file_serializer = self.input_serializer(data=request.data)
        if file_serializer.is_valid():
            validated_data = file_serializer.validated_data
            validated_xml = validated_data.get('file')
            elapsed_time = validated_data.get('time_elapsed_in_days')

            yak_details = CustomXMLParser().parse(validated_xml.read())
            yaks, yak_serializer_errors = get_or_create_herd_details(yak_details,
                                                                     delete_existing=False,
                                                                     save_to_database=False)
            if yak_serializer_errors:
                return Response({'errors': yak_serializer_errors}, status=status.HTTP_400_BAD_REQUEST)

            stock = Stock(time_elapsed_in_days=elapsed_time, yaks=yaks)

            context = dict(time_elapsed_in_days=elapsed_time)
            status_serializer = self.output_serializer(dict(herd=yaks, stock=stock),
                                                       context=context)
            return Response({'serializer': status_serializer, 'data': status_serializer.data},
                            status=status.HTTP_200_OK)
        else:
            return Response({'errors': file_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class LoadView(views.APIView):
    permission_classes = (AllowAny,)
    parser_classes = (CustomXMLParser,)

    def post(self, request, *args, **kwargs):
        yak_details = request.data
        yaks, yak_serializer_errors = get_or_create_herd_details(yak_details)
        if yak_serializer_errors:
            return Response(yak_serializer_errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_205_RESET_CONTENT)


class HerdView(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        time_elapsed_in_days = kwargs.get('time_elapsed_in_days')
        yaks = LabYak.objects.all()
        context = dict(time_elapsed_in_days=time_elapsed_in_days)
        serializer = HerdSerializer(dict(herd=yaks), context=context)
        return Response(serializer.data, status=status.HTTP_200_OK)


class StockView(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        time_elapsed_in_days = kwargs.get('time_elapsed_in_days')
        yaks = LabYak.objects.all()
        stock = Stock(time_elapsed_in_days=time_elapsed_in_days, yaks=yaks)
        serializer = StockSerializer(stock)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OrderView(views.APIView):
    permission_classes = (AllowAny,)
    input_serializer = OrderInputSerializer
    output_serializer = OrderOutputSerializer
    renderer_classes = [TemplateHTMLRenderer]

    def get(self, request, *args, **kwargs):
        time_elapsed_in_days = kwargs.get('time_elapsed_in_days')
        serializer = self.input_serializer()
        return Response({'input_serializer': serializer,
                        'time_elapsed_in_days': time_elapsed_in_days},
                        template_name='order.html')

    def post(self, request, *args, **kwargs):
        time_elapsed_in_days = kwargs.get('time_elapsed_in_days')
        yaks = LabYak.objects.all()
        stock = Stock(time_elapsed_in_days=time_elapsed_in_days,
                      yaks=yaks)
        context = dict(stock=stock,
                       time_elapsed_in_days=time_elapsed_in_days)
        input_serializer = self.input_serializer(data=request.data, context=context)
        if input_serializer.is_valid():
            order = input_serializer.save()
            STATUS_CODE = 206
            partial = "Partial"
            if order is None:
                STATUS_CODE = 404
                return Response({'order': order,
                                 'time_elapsed_in_days': time_elapsed_in_days},
                                template_name='order-detail.html', status=STATUS_CODE)
            elif order.milk and order.skins:
                STATUS_CODE = 201
                partial = ""
            output_serializer = self.output_serializer(order)
            return Response({'serializer': output_serializer,
                             'order': order,
                             'time_elapsed_in_days': time_elapsed_in_days,
                             'partial': partial},
                            template_name='order-detail.html',
                            status=STATUS_CODE)
        else:
            return Response({'input_serializer': input_serializer,
                            'time_elapsed_in_days': time_elapsed_in_days},
                            template_name='order.html',
                            status=status.HTTP_400_BAD_REQUEST)
