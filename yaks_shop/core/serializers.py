from collections import OrderedDict
from decimal import Decimal

from rest_framework import serializers
from django.core.validators import MinValueValidator
from .models import FileUpload, LabYak, Order


class LabYakSerializer(serializers.ModelSerializer):
    class Meta:
        model = LabYak
        fields = ['name', 'age', 'sex']


class LabYakDetailsOutputSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    age_last_shaved = serializers.SerializerMethodField()
    age = serializers.SerializerMethodField()

    class Meta:
        model = LabYak
        fields = ['name', 'age', 'age_last_shaved']

    def get_age_last_shaved(self, obj):
        time_elapsed_in_days = self.context.get('time_elapsed_in_days')
        age_last_shaved_in_days = obj.get_wool_skins_and_age_last_shaved_in_days(time_elapsed_in_days)[1]
        return obj.get_age_in_yak_years(age_last_shaved_in_days)

    def get_age(self, obj):
        time_elapsed_in_days = self.context.get('time_elapsed_in_days')
        current_age_in_days = obj.get_age_in_days(time_elapsed_in_days)
        return obj.get_age_in_yak_years(current_age_in_days)


class StockSerializer(serializers.Serializer):
    milk = serializers.DecimalField(max_digits=9, decimal_places=3,
                                    validators=[MinValueValidator(Decimal('0.01'))],
                                    style={'placeholder': 'Milk', 'autofocus': True})
    skins = serializers.IntegerField(validators=[MinValueValidator(1)],
                                     style={'placeholder': 'Skins', 'autofocus': True})


class HerdSerializer(serializers.Serializer):
    herd = serializers.ListField(read_only=True, child=LabYakDetailsOutputSerializer())


class StatusOutputSerializer(serializers.Serializer):
    herd = serializers.ListField(read_only=True, child=LabYakDetailsOutputSerializer())
    stock = StockSerializer()


class FileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileUpload
        fields = ['file', 'time_elapsed_in_days']


class OrderInputSerializer(serializers.Serializer):

    customer = serializers.CharField(
        max_length=100,
        style={'placeholder': 'Customer', 'autofocus': True}
    )
    order = StockSerializer()

    class Meta:
        fields = ['customer', 'order']

    def save(self):
        order = None
        customer = self.validated_data.get('customer')
        requested_stock = self.validated_data.get('order')
        current_stock = self.context.get('stock')
        time_elapsed_in_days = self.context.get('time_elapsed_in_days')
        order_dict = dict(time_elapsed_in_days=time_elapsed_in_days,
                          customer_name=customer)
        if current_stock.milk >= requested_stock.get('milk'):
            order_dict['milk'] = requested_stock.get('milk')
        if current_stock.skins >= requested_stock.get('skins'):
            order_dict['skins'] = requested_stock.get('skins')
        if order_dict.get('milk') or order_dict.get('skins'):
            order = Order.objects.create(**order_dict)
        return order


class OrderOutputSerializer(serializers.ModelSerializer):
    milk = serializers.DecimalField(max_digits=9, decimal_places=3, read_only=True)
    skins = serializers.IntegerField(read_only=True)

    class Meta():
        model = Order
        fields = ['milk', 'skins']

    def to_representation(self, instance):
        result = super(OrderOutputSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])
