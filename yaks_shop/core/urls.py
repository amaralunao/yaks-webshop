from django.urls import path

from .views import FileUploadView, HerdView, LoadView, OrderView, StockView

urlpatterns = [
    path('load/', LoadView.as_view(), name='load'),
    path('upload-file/', FileUploadView.as_view(), name='upload-file'),
    path('herd/<int:time_elapsed_in_days>/', HerdView.as_view(), name='herd'),
    path('stock/<int:time_elapsed_in_days>/', StockView.as_view(), name='stock'),
    path('order/<int:time_elapsed_in_days>/', OrderView.as_view(), name='order'),
]
