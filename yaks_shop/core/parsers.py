import untangle
import xml.sax

from rest_framework.exceptions import ParseError
from rest_framework.parsers import BaseParser


class CustomXMLParser(BaseParser):
    """
    Custom xml parser.
    """

    media_type = 'application/xml'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Parses the incoming bytestream as XML and returns the resulting data.
        """
        try:
            if isinstance(stream, bytes):
                data = untangle.parse(stream.decode())
            else:
                data = untangle.parse(stream.read().decode())
            data.herd.labyak
        except (ValueError, AttributeError, xml.sax.SAXParseException) as exc:
            raise ParseError('XML parse error - %s' % str(exc))
        return data
