from decimal import Decimal

from django.core.validators import FileExtensionValidator
from django.db import models

TWO_PLACES = Decimal('0.01')
THREE_PLACES = Decimal('0.001')


class LabYak(models.Model):
    GENDER_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    sex = models.CharField(max_length=1, choices=GENDER_CHOICES)
    name = models.CharField(max_length=30)
    age = models.DecimalField(max_digits=4, decimal_places=2)

    def __str__(self):
        return f"{self.name}"

    def get_age_in_days(self, time_elapsed_in_days):
        if self.is_alive_on_day(int(self.age * 100 + time_elapsed_in_days)):
            return int(self.age * 100 + time_elapsed_in_days)
        else:
            return 1000  # Freeze age at yak's death

    @staticmethod
    def get_age_in_yak_years(current_age_in_days):
        return Decimal(current_age_in_days * 0.01).quantize(TWO_PLACES)

    @staticmethod
    def is_old_enough_to_shave_on_day(current_age_in_days):
        return current_age_in_days > 100

    @staticmethod
    def is_alive_on_day(current_age_in_days):
        return current_age_in_days < 1000

    def is_eligible_for_shave(self, age_in_days, age_last_shaved_in_days):
        is_eligible_for_shave = False
        days_between_shaving = 8 + age_last_shaved_in_days * 0.01

        if self.is_alive_on_day(age_in_days) and self.is_old_enough_to_shave_on_day(age_in_days):
            is_eligible_for_shave = float((age_in_days - age_last_shaved_in_days)) > days_between_shaving
        return is_eligible_for_shave

    def get_wool_skins_and_age_last_shaved_in_days(self, time_elapsed_in_days):
        age_last_shaved_in_days = self.get_age_in_days(0)
        wool_skins = 0

        if time_elapsed_in_days == 0:
            return (wool_skins, age_last_shaved_in_days)

        if self.is_old_enough_to_shave_on_day(self.get_age_in_days(0)):
            wool_skins = 1

        for elapsed_day in range(1, time_elapsed_in_days):
            current_age_in_days = self.get_age_in_days(0) + elapsed_day
            if self.is_eligible_for_shave(current_age_in_days, age_last_shaved_in_days):
                wool_skins += 1
                age_last_shaved_in_days = current_age_in_days
        return (wool_skins, age_last_shaved_in_days)

    def is_female(self):
        return self.sex == 'f'

    def is_eligible_for_milking(self, current_age_in_days):
        return self.is_female() and self.is_alive_on_day(current_age_in_days)

    def get_milk_per_day(self, current_age_in_days):

        milk_quantity = Decimal(0)
        if self.is_eligible_for_milking(current_age_in_days):
            milk_quantity = Decimal(50 - current_age_in_days * 0.03)
        return milk_quantity.quantize(THREE_PLACES)

    def get_total_milk_quantity(self, time_elapsed_in_days):
        THREE_PLACES = Decimal('0.001')

        total_milk_quantity = Decimal(0)
        for elapsed_day in range(0, time_elapsed_in_days):
            current_age_in_days = self.get_age_in_days(elapsed_day)
            total_milk_quantity += self.get_milk_per_day(current_age_in_days)
        return total_milk_quantity.quantize(THREE_PLACES)


class Stock:

    def __init__(self, time_elapsed_in_days=0, yaks=None, milk=None, skins=None):
        self.time_elapsed_in_days = time_elapsed_in_days
        self.yaks = yaks if yaks else []
        stock = self.get_stock()
        self.milk = milk or stock.get('milk')
        self.skins = skins or stock.get('skins')

    def get_stock(self):
        liters_of_milk_in_stok = Decimal(0)
        wool_skins = 0
        for yak in self.yaks:
            liters_of_milk_in_stok += yak.get_total_milk_quantity(self.time_elapsed_in_days)
            wool_skins += yak.get_wool_skins_and_age_last_shaved_in_days(self.time_elapsed_in_days)[0]
        return dict(milk=liters_of_milk_in_stok, skins=wool_skins)


class FileUpload(models.Model):
    file = models.FileField(blank=False, null=False,
                            validators=[FileExtensionValidator(allowed_extensions=['xml'])])
    time_elapsed_in_days = models.PositiveIntegerField(blank=False, null=False)


class Order(models.Model):
    customer_name = models.CharField(max_length=30)
    time_elapsed_in_days = models.PositiveIntegerField()
    milk = models.DecimalField(max_digits=9, decimal_places=3, null=True, blank=True)
    skins = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return f"customer: {self.customer_name}, milk: {self.milk}, skins: {self.skins}"
