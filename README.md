# yaks-webshop (assignment)

[![Built with](https://img.shields.io/badge/Built_with-Cookiecutter_Django_Rest-F7B633.svg)](https://github.com/agconti/cookiecutter-django-rest)

A REST web-shop API that allows loading and querying of information related to a yak herd and placing of yak products orders by customers.

# Prerequisites

- [Docker](https://docs.docker.com/docker-for-mac/install/)  
- [Development environment with Python 3.7+](https://www.python.org/downloads/release/python-370/)
- [Virtualenv](https://virtualenv.pypa.io/en/latest/)

# Local Development (with docker)

Start the dev server for local development:
```bash
docker-compose up
```

Run a command inside the docker container:

```bash
docker-compose run --rm web [command]
```


Note: Sqlite when developing locally was used so be sure to replace current sqlite value of DATABASES from `yaks_shop/config/common.py` with Postgres config:

```
DATABASES = {
        'default': dj_database_url.config(
            default='postgres://postgres:@postgres:5432/postgres',
            conn_max_age=int(os.getenv('POSTGRES_CONN_MAX_AGE', 600))
        )
    }
```


# Local Development (without docker)

Create & activate a virtualenv:

```
python3 -m venv env && source env/bin/activate
```

Install project requirements:

```
pip install -r requirements.txt
```

Create tables in local development sqlite database:

```
./manage.py migrate
```

Create a superuser in your local database in order to access the `/admin/` page:

```
./manage.py createsuperuser
```
You will be prompted for username, email and password at this step.

Finally, run:

```
./manage.py runserver
```
to start a local development server at http://127.0.0.1:8000/

# Calling endpoints

## 1. File Upload Endpoint

- This endpoint needs to be tested with postman.
[How to upload a file with postman](https://stackoverflow.com/questions/39037049/how-to-upload-a-file-and-json-data-in-postman)

- URL
http://127.0.0.1:8000/yak-shop/upload-file/

- Method:
| POST |

- URL Params

None

- Data Params

Required:

file=[file.xml]

time_elapsed_in_days=[integer]

- Success Response:

(This will not create new entries in the database and will not delete existing ones.)

Code: 200 

Content example:
```
In Stock:

    1104.480 liters of milk
    3 skins of wool
Herd:

    Betty-1 4.13 years old
    Betty-2 8.13 years old
    Betty-3 9.63 years old
```

- Error Response:

Code: 400 BAD REQUEST

Content example:

```
Input Error: Please check your input data

In Stock:

liters of milk
skins of wool
Herd:
```

## 2. Load Endpoint


- URL
http://127.0.0.1:8000/yak-shop/load/

- Method:
| POST |

- URL Params

None

- Data Params

Required:

(Can be done in browser or in postman with raw xml format)
xml data of format:

```
<herd>
    <labyak name="Betty-1" age="dd" sex="f"/>
    <labyak name="Betty-2" age="8" sex="f"/>
    <labyak name="Betty-3" age="9.5" sex="f"/>
</herd>
```

- Success Response:

Code: 205 RESET CONTENT 

Content:
None

(This will create new entries in the database and delete existing ones.)

- Error Response:

(In case of validation error with the data from the xml provided.)

Code: 400 BAD REQUEST

Content example:

```
[
    {
        "age": [
            "A valid number is required."
        ]
    }
]
```

## 3. Order Endpoint


- URL
http://127.0.0.1:8000/yak-shop/order/13/

- Method:
| POST |

- URL Params

Required:

time_elapsed_in_days=[integer]

- Data Params

Required:

First way: In Browser with a user interface with a form to fill in.
All fields are mandatory and will display validation errors

Second way: With Postman:

json payload

dict(customer=[string]
     order=dict(milk=[decimal]
                skins=[integer]))

Payload Example:
```
 {'customer': 'Medvedev',
  'order':
        {'milk': 1100,
        'skins': 3
        }
    }
```

- Success Response:

Code: 201 CREATED 

Content example:
 
```
Order number 44 has been placed on day 14 for:
1100.00 liters of milk
3 skins of wool
```

Code: 206 PARTIAL CONTENT

Content example:

```
Partial Order number 45 has been placed on day 14 for:
2 skins of wool
```

- Error Response:

Code: 404 NOT FOUND

Content example:

```
Order failed because stock is insufficient
```


Code: 400 BAD REQUEST

Form validation will indicate the field errors.


## 4. Herd Endpoint

- URL
http://127.0.0.1:8000/yak-shop/herd/14/

- Method:
| GET |

- URL Params

Required:

time_elapsed_in_days=[integer]

- Data Params
None

- Success Response:

Code: 200 OK 

Content example:
 
```
{
    "herd": [
        {
            "name": "Betty-1",
            "age": 4.14,
            "age_last_shaved": 4.13
        },
        {
            "name": "Betty-2",
            "age": 8.14,
            "age_last_shaved": 8.0
        },
        {
            "name": "Betty-3",
            "age": 9.64,
            "age_last_shaved": 9.5
        }
    ]
}
```
or empty herd list in case of no entries in database.

## 5. Stock Endpoint

- URL
http://127.0.0.1:8000/yak-shop/stock/14/

- Method:
| GET |

- URL Params

Required:

time_elapsed_in_days=[integer]

- Data Params
None

- Success Response:

Code: 200 OK 

Content example:
 
```
{
    "milk": "1188.810",
    "skins": 4
}
```


## 6. Admin Endpoints for seeing database LabYak and Order entries

- URLS:

http://127.0.0.1:8000/admin/core/order/


http://127.0.0.1:8000/admin/core/labyak/

(Authentication with the superuser credentials created previously is needed for this step)


# Running tests

```
./manage.py test
```


